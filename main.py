# -*- coding: utf-8 -*-
import re
import urllib.request


import random
import datetime
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes.blocks import *
from slack.web.classes import extract_json

SLACK_TOKEN = 'xoxb-689227472356-694319267511-tfcPfZ4K9qYQF1OTZcEp4lyR'
SLACK_SIGNING_SECRET = '53192b6c047b22e292afb57f22f14b82'


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

count = 1
today = 'today.txt'
mon = 'mon.txt'
tue ='tue.txt'
wed='wed.txt'
thu ='thu.txt'
fri ='fri.txt'

food_list = ['백반', '쌈밥', '면', '돈가스', '라멘', '스시', '스테이크','파스타', '햄버거']

# 크롤링 함수 구현하기
def _yes_no(text):
    keywords = []
    global count
    if "응" in text:
        keywords = "무슨 요일 식단 볼래?\n오늘 | 월 | 화 | 수 | 목 | 금"
        return keywords
    elif "아니" in text:
        count = 3
        keywords = "주변식당 어떻게 찾을래?\n검색 | 추천"
        return keywords
    else:
        keywords = "다시 입력해줘\n응 |아니"
        count = 1
    return ''.join(keywords)

def _yes_mon(text):
    keywords = []
    with open(text) as file:
                keywords = ''.join(file)
    keywords = keywords.split(":")
    keywords = keywords[1].replace("@","\n")
    keywords = keywords.replace("-", "\n")
    return keywords

def _yes_tue(text):
    keywords = []
    with open(text) as file:
                keywords = ''.join(file)
    keywords = keywords.split(":")
    keywords = keywords[1].replace("@", "\n")
    keywords = keywords.replace("-", "\n")
    return keywords

def _yes_wed(text):
    keywords = []
    with open(text) as file:
                keywords = ''.join(file)
    keywords = keywords.split(":")
    keywords = keywords[1].replace("@", "\n")
    keywords = keywords.replace("-", "\n")
    return keywords

def _yes_thu(text):
    keywords = []
    with open(text) as file:
                keywords = ''.join(file)
    keywords = keywords.split(":")
    keywords = keywords[1].replace("@", "\n")
    keywords = keywords.replace("-", "\n")
    return keywords

def _yes_fri(text):
    keywords = []
    with open(text) as file:
                keywords = ''.join(file)
    keywords = keywords.split(":")
    keywords = keywords[1].replace("@", "\n")
    keywords = keywords.replace("-", "\n")
    return keywords


def _yes_today(text):

    keywords = []

    with open(text) as file:
            for line in file:
                keywords.append(line)
    keywords = ''.join(keywords)
    keywords = keywords.split('.')

    dt = datetime.datetime.now()
    for j in keywords:
        j = j.strip().split(":")
        print(j[0])
        if int(j[0]) == dt.weekday():
            return j[1]

def _food_random(text):
    global count
    if "추천" == text:
        randomfood = random.choice(food_list)
        if randomfood== "백반":
            keywords = "<https://www.google.com/search?client=firefox-b-d&tbm=lcl&ei=eQkoXabKFIXemAW1pIDgAQ&q=%EB%B0%B1%EB%B0%98&oq=%EB%B0%B1%EB%B0%98&gs_l=psy-ab.3..0l6j0i3k1j0l3.1140.4083.0.4220.6.6.0.0.0.0.169.760.1j5.6.0....0...1c.4.64.psy-ab..3.3.396....0.lZDy0ohpSh4#rlfi=hd:;si:;mv:!1m2!1d37.5361943!2d127.06102650000001!2m2!1d37.486557999999995!2d127.0049448!3m12!1m3!1d24066.071858311963!2d127.03298565!3d37.51137615!2m3!1f0!2f0!3f0!3m2!1i327!2i365!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 백반 찾기>"  + "\n다시 하시려면 처음 | 끝"
            count = 8
        if randomfood == "쌈밥":
            keywords = "<https://www.google.com/search?client=firefox-b-d&tbm=lcl&ei=fgkoXbjGK5PemAXjuo-wDg&q=%EC%8C%88%EB%B0%A5&oq=%EC%8C%88%EB%B0%A5&gs_l=psy-ab.3.0.0l10.18339.18976.0.20288.6.6.0.0.0.0.125.527.2j3.5.0....0...1c.1j4.64.psy-ab..2.4.427...0i131k1.0.CJM9n50beLo#rlfi=hd:;si:;mv:!1m2!1d37.5784866!2d127.08665479999999!2m2!1d37.4615507!2d126.9549772!3m12!1m3!1d56697.05493622787!2d127.02081600000001!3d37.52001865!2m3!1f0!2f0!3f0!3m2!1i384!2i430!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 쌈밥 찾기>" + "\n다시 하시려면 처음 | 끝"
            count = 8
        if randomfood == "면":
            keywords = "<https://www.google.com/search?client=firefox-b-d&tbm=lcl&ei=kwkoXbvDM4SNr7wP0Ia92Ag&q=%EB%A9%B4&oq=%EB%A9%B4&gs_l=psy-ab.3..0l10.20898.21101.0.21223.3.3.0.0.0.0.121.309.2j1.3.0....0...1c.1j4.64.psy-ab..1.2.216...0i131k1.0.UtcD5JyjwP0#rlfi=hd:;si:;mv:!1m2!1d37.5740729!2d127.07477480000001!2m2!1d37.4803591!2d126.91189689999999!3m12!1m3!1d45419.18886767893!2d126.99333585000001!3d37.527216!2m3!1f0!2f0!3f0!3m2!1i949!2i689!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 면 찾기>" + "\n다시 하시려면 처음 | 끝"
            count = 8
        if randomfood == "돈가스":
            keyword = "<https://www.google.com/search?client=firefox-b-d&tbm=lcl&ei=qgkoXa7LCtuAr7wP05O_wA8&q=%EB%8F%88%EA%B0%80%EC%8A%A4&oq=%EB%8F%88%EA%B0%80%EC%8A%A4&gs_l=psy-ab.3..0l7j0i10k1l3.29255.29842.0.30021.8.7.1.0.0.0.151.886.1j6.7.0....0...1c.1j4.64.psy-ab..5.3.279...0i131k1.0.JrlqDEoDzKM#rlfi=hd:;si:;mv:!1m2!1d37.525928199999996!2d127.08885589999998!2m2!1d37.4801662!2d127.00825259999998!3m12!1m3!1d22189.415833993!2d127.04855424999998!3d37.5030472!2m3!1f0!2f0!3f0!3m2!1i940!2i673!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 돈가스 찾기>" + "\n다시 하시려면 처음 | 끝"
            count =8
        if randomfood == "라멘":
            keyword = "<https://www.google.com/search?client=firefox-b-d&tbm=lcl&ei=yAkoXeCtM6mxmAW987L4Dw&q=%EB%9D%BC%EB%A9%98&oq=%EB%9D%BC%EB%A9%98&gs_l=psy-ab.3..0l10.15794.16118.0.16203.5.4.0.0.0.0.172.381.1j2.3.0....0...1c.4.64.psy-ab..4.1.112....0.H9oiOZmep-E#rlfi=hd:;si:;mv:!1m2!1d37.532148899999996!2d127.0557585!2m2!1d37.493560699999996!2d127.00368239999999!3m12!1m3!1d18692.044399646675!2d127.02972044999999!3d37.5128548!2m3!1f0!2f0!3f0!3m2!1i607!2i567!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 라멘 찾기>" + "\n다시 하시려면 처음 | 끝"
            count = 8
        if randomfood == "스시":
            keyword = "<https://www.google.com/search?client=firefox-b-d&tbm=lcl&ei=jAsoXbT1FcaUr7wPzI-RsAo&q=%EC%8A%A4%EC%8B%9C&oq=%EC%8A%A4%EC%8B%9C&gs_l=psy-ab.12...0.0.0.4537.0.0.0.0.0.0.0.0..0.0....0...1c..64.psy-ab..0.0.0....0.j4ADslOlpo0#rlfi=hd:;si:;mv:!1m2!1d37.527239099999996!2d127.04115039999999!2m2!1d37.497183899999996!2d127.0206067!3m12!1m3!1d14571.34891704126!2d127.03087854999998!3d37.5122115!2m3!1f0!2f0!3f0!3m2!1i240!2i442!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 스시 찾기>" + "\n다시 하시려면 처음 | 끝"
            count =8
        if randomfood == "스테이크":
            keywords = "<https://www.google.com/search?client=firefox-b-d&tbm=lcl&ei=kgsoXbDVA63VmAWp372oAQ&q=%EC%8A%A4%ED%85%8C%EC%9D%B4%ED%81%AC&oq=%EC%8A%A4%ED%85%8C%EC%9D%B4%ED%81%AC&gs_l=psy-ab.3..0l10.21687.22457.0.22615.11.8.3.0.0.0.135.763.2j5.7.0....0...1c.1j4.64.psy-ab..6.5.270...0i131k1.0.VR7m5qf8I3A#rlfi=hd:;si:;mv:!1m2!1d37.5358424!2d127.06198669999999!2m2!1d37.4927262!2d126.9894903!3m12!1m3!1d20900.404170252925!2d127.02573849999999!3d37.5142843!2m3!1f0!2f0!3f0!3m2!1i845!2i634!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 스테이크 찾기>" + "\n다시 하시려면 처음 | 끝"
            count = 8
        if randomfood == "파스타":
            keywords = "<https://www.google.com/search?client=firefox-b-d&tbm=lcl&ei=qQsoXY7FF6uymAX9-5jgBQ&q=%ED%8C%8C%EC%8A%A4%ED%83%80&oq=%ED%8C%8C%EC%8A%A4%ED%83%80&gs_l=psy-ab.3..0l10.14241.14785.0.15054.7.6.1.0.0.0.140.674.3j3.6.0....0...1c.1j4.64.psy-ab..3.4.407...0i131k1j0i67k1.0.oY3-NXjHfPg#rlfi=hd:;si:;mv:!1m2!1d37.5391851!2d127.04260509999997!2m2!1d37.4911317!2d126.99171899999999!3m12!1m3!1d23273.67905219651!2d127.01716204999998!3d37.5151584!2m3!1f0!2f0!3f0!3m2!1i593!2i706!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 파스타 찾기>" + "\n다시 하시려면 처음 | 끝"
            count = 8
        if randomfood == "햄버거":
            keywords = "<https://www.google.com/search?client=firefox-b-d&tbm=lcl&ei=uQsoXeuQG4S1mAX3sar4CA&q=%ED%96%84%EB%B2%84%EA%B1%B0&oq=%ED%96%84%EB%B2%84%EA%B1%B0&gs_l=psy-ab.3..0l10.14858.16004.0.16287.8.7.1.0.0.0.180.782.4j3.7.0....0...1c.1j4.64.psy-ab..2.6.587....0.y0Yo5L1RJAU#rlfi=hd:;si:;mv:!1m2!1d37.5294698!2d127.05873340000001!2m2!1d37.4877278!2d126.99991840000001!3m12!1m3!1d20242.627327406404!2d127.02932590000002!3d37.50859880000001!2m3!1f0!2f0!3f0!3m2!1i686!2i614!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 햄버거 찾기>" + "\n다시 하시려면 처음 | 끝"
            count = 8

    return keywords



# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):

    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    image = "http://edu.ssafy.com/data/upload_files/namo/images/000004/20190708082304576_18S6XNQP.png"
    start = "\n식당서 밥 먹을거야? \n 응 | 아니"


    start3 = "하나 골라봐!\n한식 | 중식 | 양식"
    start4 = "하나 골라봐!\n백반 | 쌈밥 | 면"
    start5 = "하나 골라봐!\n돈가스 | 라멘 | 스시"
    start6 = "하나 골라봐!\n스테이크 | 파스타 | 햄버거"

    global count

    if count == 1:
        block1 = SectionBlock(            text=" "        )
        block3 = ImageBlock(
            image_url="http://edu.ssafy.com/data/upload_files/namo/images/000004/20190708082304576_18S6XNQP.png",
            alt_text="이미지가 안 보일 때 대신 표시할 텍스트"
        )

        # 여러 개의 블록을 하나의 메시지로 묶어 보냅니다.
        my_blocks = [block1, block3]
        slack_web_client.chat_postMessage(
            channel="12조",
            blocks=extract_json(my_blocks)
        )
        keywords = start
    elif count == 2:
        keywords = _yes_no(text)
    elif count == 3:
        text = text[13:]
        print(text)
        if text == "월":
            keywords = _yes_mon(mon) + "\n다시 하시려면\n\n처음 | 끝"
            count = 8
        elif text == "화":
             keywords = _yes_tue(tue) + "\n다시 하시려면\n\n처음 | 끝"
             count = 8
        elif text == "수":
            keywords = _yes_wed(wed) + "\n다시 하시려면\n\n처음 | 끝"
            count = 8
        elif text == "목":
             keywords = _yes_thu(thu) + "\n다시 하시려면\n\n처음 | 끝"
             count = 8
        elif text == "금":
            keywords = _yes_fri(fri)+ "\n다시 하시려면\n\n처음 | 끝"
            count = 8
        elif text == "오늘":
            keywords = _yes_today(today) + "\n다시 하시려면\n\n처음 | 끝"
            count = 8
        else:
            count = 2
            keywords = "다시 입력해줘\n 월|화|수|목|금|오늘"
    elif count == 4:
        text = text[13:]
        if text == "검색":
            keywords = start3
            count = 4
        elif text == "추천":
            keywords = _food_random(text)
        else:
            keywords = "다시 입력 해줘\n 검색|추천"
            count = 3
    elif count == 5:
        text = text[13:]
        print(text)
        if text == '한식':
            keywords = start4
            count = 5
        elif text == '중식':
            keywords = start5
            count = 6
        elif text == '일식':
            keywords = start6
            count = 7
        else:
            keywords = "다시 입력해줘\n한식 | 중식 | 일식"
            count = 4

    elif count == 6:
        text = text[13:]
        if text == "백반":
            keywords = "<https://www.google.com/search?tbm=lcl&ei=ijQnXfXND4uN8wWysoIo&q=%EB%B0%B1%EB%B0%98&oq=%EB%B0%B1%EB%B0%98&gs_l=psy-ab.3..0l6j0i3k1j0l3.984.1462.0.1830.6.4.0.0.0.0.364.845.0j1j1j1.3.0....0...1c.1j4.64.psy-ab..4.2.558...0i20i263k1j0i131k1.0.6AbMBH9tkqc#rlfi=hd:;si:;mv:!1m2!1d37.6692332!2d127.0536118!2m2!1d37.5514164!2d126.91456099999999!3m12!1m3!1d57155.14016701143!2d126.98408640000001!3d37.61032480000001!2m3!1f0!2f0!3f0!3m2!1i406!2i434!4f13.1;tbs:lrf:!2m1!1e3!3sIAE,lf:1,lf_ui:9| 백반 찾기>"  + "\n다시 하시려면 처음 | 끝"
            count = 8
        elif text == "쌈밥":
            keywords = "<https://www.google.com/search?tbm=lcl&ei=jDQnXefNMsix8wW5grTgCg&q=%EC%8C%88%EB%B0%A5&oq=%EC%8C%88%EB%B0%A5&gs_l=psy-ab.3..0j0i131k1j0l8.78543.80927.0.81062.12.10.1.0.0.0.137.551.1j4.6.0....0...1c.1j4.64.psy-ab..6.6.547.10..0i10i42k1j0i10k1j35i39k1.99.9qQ98rShJtU#rlfi=hd:;si:;mv:!1m2!1d37.6936988!2d127.1103528!2m2!1d37.5653462!2d126.9230343!3m12!1m3!1d62275.118456560005!2d127.01669355!3d37.6295225!2m3!1f0!2f0!3f0!3m2!1i546!2i473!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 쌈밥 찾기>" + "\n다시 하시려면 처음 | 끝"
            count = 8
        elif text == "면":
            keywords = "<https://www.google.com/search?sz=0&tbm=lcl&ei=F-QnXYb8NeukmAWh7buAAg&q=%EB%A9%B4&oq=%EB%A9%B4&gs_l=psy-ab.3..35i39k1j0l9.1292.1515.0.1656.3.3.0.0.0.0.133.252.0j2.2.0....0...1c.4.64.psy-ab..1.2.251...0i131k1.0.I8mULBdBrKc#rlfi=hd:;si:;mv:!1m2!1d37.674894099999996!2d127.1079796!2m2!1d37.4958399!2d126.89641739999999!3m12!1m3!1d86815.37663166564!2d127.00219849999999!3d37.585367!2m3!1f0!2f0!3f0!3m2!1i617!2i659!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 면 찾기>" + "\n다시 하시려면 처음 | 끝"
            count = 8
        else:
            count = 5
            keywords ="다시 입력 해줘\n 백반 | 쌈밥| 면"

    elif count == 7:
        text = text[13:]
        if text == "돈가스":
             keyword = "<https://www.google.com/search?tbm=lcl&ei=_DQnXZGbEbaTr7wP2duh2Aw&q=%EB%8F%88%EA%B0%80%EC%8A%A4&oq=%EB%8F%88%EA%B0%80%EC%8A%A4&gs_l=psy-ab.3..0l7j0i10k1l3.754278.757718.0.757898.17.15.2.0.0.0.150.1400.2j10.13.0....0...1c.1j4.64.psy-ab..4.13.1293.10..0i131k1j35i39k1j0i13k1.99.ltjVinLRauc#rlfi=hd:;si:;mv:!1m2!1d37.6687947!2d127.0819822!2m2!1d37.5522145!2d126.93169319999998!3m12!1m3!1d56496.53422560453!2d127.0068377!3d37.6105046!2m3!1f0!2f0!3f0!3m2!1i438!2i429!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 돈가스 찾기>" + "\n다시 하시려면 처음 | 끝"
             count = 8
        elif text == "라멘":
             keyword = "<https://www.google.com/search?tbm=lcl&ei=8zcnXbWTFq62mAWUs5PQCg&q=%EB%9D%BC%EB%A9%98&oq=%EB%9D%BC%EB%A9%98&gs_l=psy-ab.3..35i39k1l10.9635.11447.0.11523.15.12.3.0.0.0.146.1045.1j8.10.0....0...1c.1j4.64.psy-ab..6.9.618.10..0j0i131k1j0i20i263k1j0i10i42k1j0i3k1j0i10k1.96.7MicAdE8Bww#rlfi=hd:;si:;mv:!1m2!1d37.7315598!2d127.08492190000001!2m2!1d37.5469393!2d126.96158539999999!3m12!1m3!1d89516.99278888594!2d127.02325364999999!3d37.63924955!2m3!1f0!2f0!3f0!3m2!1i180!2i340!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 라멘찾기>" + "\n다시 하시려면 처음 | 끝"
             count = 8
        elif text == "스시":
             keyword = "<https://www.google.com/search?tbm=lcl&ei=ADgnXbb2EY6Er7wP2ZKGKA&q=%EC%8A%A4%EC%8B%9C&oq=%EC%8A%A4%EC%8B%9C&gs_l=psy-ab.3..0l4j0i131k1j0l5.22350.26057.0.26217.16.12.3.0.0.0.136.1149.0j10.11.0....0...1c.1j4.64.psy-ab..4.11.896.10..35i39k1j0i67k1j0i20i263k1j0i10k1.100.d87kqwbrIcQ#rlfi=hd:;si:;mv:!1m2!1d37.662031999999996!2d127.08233089999999!2m2!1d37.6004565!2d127.0090011!3m12!1m3!1d29820.269670132573!2d127.045666!3d37.63124425!2m3!1f0!2f0!3f0!3m2!1i428!2i453!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 스시 찾기>" + "\n다시 하시려면 처음 | 끝"
             count = 8
        else:
            count = 6
            keywords = "다시 입력 해줘\n돈가스 | 라멘| 스시"

    elif count == 8:
        text = text[13:]
        if text =="스테이크":
            keywords = "<https://www.google.com/search?tbm=lcl&ei=HDgnXZvjFs6smAWHvqHICQ&q=%EC%8A%A4%ED%85%8C%EC%9D%B4%ED%81%AC&oq=%EC%8A%A4%ED%85%8C%EC%9D%B4%ED%81%AC&gs_l=psy-ab.3..0l10.8856.10752.0.10978.16.13.3.0.0.0.180.1369.0j10.11.0....0...1c.1j4.64.psy-ab..5.11.1101.10..0i131k1j0i10i67k1j0i20i263k1j0i10k1j35i39k1.97.i_aFvF21Vos#rlfi=hd:;si:;mv:!1m2!1d37.666701800000006!2d127.06789339999999!2m2!1d37.5149325!2d126.9187583!3m12!1m3!1d73504.45090087813!2d126.99332584999999!3d37.59081715!2m3!1f0!2f0!3f0!3m2!1i218!2i279!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,l f:1,lf_ui:9| 스테이크 찾기>" + "\n다시 하시려면 처음 | 끝"
            count = 8
        if text == "파스타":
            keywords = "<https://www.google.com/search?tbm=lcl&ei=KDgnXcXYM5WXr7wPi-epgAI&q=%ED%8C%8C%EC%8A%A4%ED%83%80&oq=%ED%8C%8C%EC%8A%A4%ED%83%80&gs_l=psy-ab.3..0i20i263k1l2j0l8.7947.10771.0.10909.18.15.3.0.0.0.141.1418.4j9.14.0....0...1c.1j4.64.psy-ab..2.16.1445.10..0i67k1j35i39k1j0i10i42k1j0i10k1j0i131k1.112.kYl-7tQiefA#rlfi=hd:;si:;mv:!1m2!1d37.6570939!2d127.0842971!2m2!1d37.5532147!2d126.9361316!3m12!1m3!1d50310.556088963465!2d127.01021435000001!3d37.6051543!2m3!1f0!2f0!3f0!3m2!1i432!2i382!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 파스타찾기>" + "\n다시 하시려면 처음 | 끝"
            count = 8
        if text == "햄버거":
            keywords = "<https://www.google.com/search?tbm=lcl&ei=NTgnXefECuukmAWh7buAAg&q=%ED%96%84%EB%B2%84%EA%B1%B0&oq=%ED%96%84%EB%B2%84%EA%B1%B0&gs_l=psy-ab.3..0l10.6673.8999.0.9124.18.13.5.0.0.0.176.1476.0j11.12.0....0...1c.1j4.64.psy-ab..6.12.925.10..35i39k1j0i131k1j0i20i263k1.122.42iJr2oIfIY#rlfi=hd:;si:;mv:!1m2!1d37.6731495!2d127.0791457!2m2!1d37.5505597!2d126.95957709999999!3m12!1m3!1d59392.71425704583!2d127.0193614!3d37.6118546!2m3!1f0!2f0!3f0!3m2!1i349!2i451!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:9| 햄버거 찾기>" + "\n다시 하시려면 처음 | 끝"
            count = 8
        else:
            count = 7
            keywords ="다시 입력 해줘\n스테이크 | 라멘 | 스시"
    elif count == 9:
        text = text[13:]
        if text == "처음":
            keywords = start
            count = 1
        elif text == "끝":
            print("끝")
            keywords = "끝났습니다."
        else:
            keywords = "다시 입력 해줘\n 처음 | 끝"
            count = 8

    slack_web_client.chat_postMessage(
        channel=channel,
        text=keywords,
    )

    count = count + 1
    print(count)



# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=8080)